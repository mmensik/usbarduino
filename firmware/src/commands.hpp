#ifndef __COMMANDS__
#define __COMMANDS__

const unsigned char ON = 1;
const unsigned char OFF = 2;
const unsigned char GET_DISTANCE = 3;
const unsigned char SET_MOTOR_POSITION = 4;

#endif
