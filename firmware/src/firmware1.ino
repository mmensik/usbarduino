#include <Stepper.h>
#include "commands.hpp"

int in1Pin = 12;
int in2Pin = 11;
int in3Pin = 10;
int in4Pin = 9;

int trigPin = 8;
int echoPin = 7;
int duration;
double distance;

int LEDPin = 2;

int motorPosition = 0;
int motorDesiredPosition = 0;
int maxStepsPerLoop = 40;

Stepper motor(512, in1Pin, in2Pin, in3Pin, in4Pin); 

void setup()
{
   Serial.begin(9600);
   
   pinMode(in1Pin, OUTPUT);
   pinMode(in2Pin, OUTPUT);
   pinMode(in3Pin, OUTPUT);
   pinMode(in4Pin, OUTPUT);
   pinMode(trigPin, OUTPUT);
   pinMode(echoPin, INPUT);
   pinMode(LEDPin, OUTPUT);
   
   motor.setSpeed(30);
}

void loop()
{
    if (Serial.available() > 0)
    {
        unsigned char cmd = Serial.read();
        switch (cmd)
        {
        case ON:
          digitalWrite(LEDPin, HIGH); 
          break;
        case OFF:
          digitalWrite(LEDPin,LOW);
          break; 
        case GET_DISTANCE:
          meassureDistance(); 
          break;
        case SET_MOTOR_POSITION:
          motorDesiredPosition = Serial.parseInt();
          break;
        }
    }
    if (motorPosition != motorDesiredPosition)
    {
        int diff = motorDesiredPosition - motorPosition;
        if (diff > maxStepsPerLoop) diff = maxStepsPerLoop;
        if (diff < -maxStepsPerLoop) diff = -maxStepsPerLoop;
        motor.step(diff);
        motorPosition += diff;
    }
    delayMicroseconds(20); 
}

void meassureDistance()
{
    digitalWrite(trigPin, LOW); 
    delayMicroseconds(2); 
   
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10); 
    
    digitalWrite(trigPin, LOW);
    duration = pulseIn(echoPin, HIGH);
    
    distance = duration/58.2;
    
    if (distance >= 200 || distance <= 0)
    {
    }
    else {
       Serial.println(distance);
    }
}
