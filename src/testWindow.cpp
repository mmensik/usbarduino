#include "testWindow.hpp"
#include "arduinoController.hpp"

#include <string>
#include <iostream>
#include <boost/thread.hpp>

#include <boost/lambda/lambda.hpp>

using namespace std;

TestWindow::TestWindow(boost::shared_ptr<ArduinoController> controller) :
    controller_(controller), 
    mainBox_(Gtk::ORIENTATION_VERTICAL, 10),
    boxButtonsH_(Gtk::ORIENTATION_HORIZONTAL, 20),
    adjustmentMotor_(Gtk::Adjustment::create(0.0, -1000.0, 1000.0, 200, 1.0, 1.0)),
    scaleMotor_(adjustmentMotor_, Gtk::ORIENTATION_HORIZONTAL)
{
    set_title("Arduino Usb Control Test");
    set_default_size(400,400);
    set_border_width(10);

    buttonLight_.set_label("Light");
    buttonMeasure_.set_label("Measure");

    buttonLight_.signal_clicked().connect(sigc::mem_fun(*this,
                                 &TestWindow::On_buttonLight_clicked) );

    buttonMeasure_.signal_clicked().connect(sigc::mem_fun(*this,
                                 &TestWindow::On_buttonMeasure_clicked) );

    adjustmentMotor_->signal_value_changed().connect(sigc::mem_fun(*this,
                                 &TestWindow::On_scaleMotor_value_change_));


    scaleMotor_.set_digits(0);
    add(mainBox_);
    boxButtonsH_.add(buttonLight_);
    boxButtonsH_.add(buttonMeasure_);
    boxButtonsH_.add(labelDistance_);
    mainBox_.add(boxButtonsH_);
    mainBox_.add(scaleMotor_);
    show_all_children();
}

void TestWindow::SetLight()
{
    controller_->SetLight(buttonLight_.get_active());
}

void TestWindow::On_buttonLight_clicked()
{
    boost::thread t(&TestWindow::SetLight, this);
}

void TestWindow::UpdateDistance()
{
    double distance = controller_->ReadDistance();
    labelDistance_.set_text(to_string(distance));
}

void TestWindow::On_buttonMeasure_clicked()
{
    boost::thread t(&TestWindow::UpdateDistance, this);
}

void TestWindow::On_scaleMotor_value_change_()
{
    controller_->SetMotorPosition((int)scaleMotor_.get_value());
}
