#include "parser.hpp"


#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>

using boost::spirit::qi::double_;
using boost::spirit::qi::_1;
using boost::spirit::qi::phrase_parse;
using boost::spirit::ascii::space;
using boost::phoenix::ref;
using boost::phoenix::push_back;

using namespace std;
using namespace boost;

bool Parser::ParseDouble(const string &str, double &result)
{
    bool state = phrase_parse(str.begin(), str.end(), double_[boost::phoenix::ref(result) = _1], space);
    return state;
}

bool Parser::ParseDoubleList(const std::string &str, std::vector<double> &v)
{
    bool state = phrase_parse(str.begin(), str.end(),
                 (
                    double_[boost::phoenix::push_back(phoenix::ref(v), _1)]
                    >> *("," >> double_[boost::phoenix::push_back(phoenix::ref(v), _1)])

                 ),
                 space);
    return state;
}
