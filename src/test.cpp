#include <parser.hpp>
#define BOOST_TEST_MODULE ArduinoTest
#include <boost/test/unit_test.hpp>

#include "hippomocks.h"

#include <String>
#include <Vector>

using namespace std;

BOOST_AUTO_TEST_CASE( stringParseTest )
{
    string str1("1.34");
    double val;
    BOOST_CHECK( Parser::ParseDouble(str1, val));
    BOOST_CHECK( val = 1.34 );

    string str2("1.34 ");
    BOOST_CHECK( Parser::ParseDouble(str2, val));
    BOOST_CHECK( val = 1.34 );

    string str3("1.34 \n");
    BOOST_CHECK( Parser::ParseDouble(str3, val));
    BOOST_CHECK( val = 1.34 );
}

BOOST_AUTO_TEST_CASE( parseVectorDouble )
{
   string str1("1.34, 3.1415, -2, .002");
   vector<double> v;
   BOOST_CHECK( Parser::ParseDoubleList(str1, v));
   BOOST_CHECK( v.size() == 4 );
   BOOST_CHECK( v[0] == 1.34 );
   BOOST_CHECK( v[1] == 3.1415 );
   BOOST_CHECK( v[2] == -2.0 );
   BOOST_CHECK( v[3] == 0.002 );
}
