#ifndef __ARDUINO_CONTROLLER__
#define __ARDUINO_CONTROLLER__

#include <String>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>

#include "arduinoSerial.hpp"
#include "commands.hpp"

class ArduinoController
{
public:
    ArduinoController(boost::shared_ptr<IArduinoSerial> serial);        

    double ReadDistance();

    void SetLight(bool lightState);

    void SetMotorPosition(int position);
private:
    boost::shared_ptr<IArduinoSerial> serial_;

    boost::mutex mtx_;
};

#endif

