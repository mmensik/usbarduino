#include "arduinoController.hpp"
#include "parser.hpp"

using namespace std;

ArduinoController::ArduinoController(boost::shared_ptr<IArduinoSerial> serial) : 
    serial_(serial)
{
}

double ArduinoController::ReadDistance()
{
    mtx_.lock();
    serial_->WriteByte(GET_DISTANCE);
    string result = serial_->ReadLine();
    mtx_.unlock();
    double ret;
    Parser::ParseDouble(result, ret);
    return ret;
}

void ArduinoController::SetLight(bool lightState)
{
    mtx_.lock();
    serial_->WriteByte(lightState ? ON : OFF);
    mtx_.unlock();
}

void ArduinoController::SetMotorPosition(int position)
{
    mtx_.lock();
    serial_->WriteByte(SET_MOTOR_POSITION);
    string posStr = to_string(position);
    serial_->Write(posStr.c_str());
    serial_->WriteByte('\n');
    mtx_.unlock();
}
