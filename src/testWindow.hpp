#ifndef _ARDUINO_TEST_WINDOWS_
#define _ARDUINO_TEST_WINDOWS_

#include <gtkmm.h>

#include <boost/shared_ptr.hpp>

class ArduinoController;

class TestWindow : public Gtk::Window
{
public:
    TestWindow(boost::shared_ptr<ArduinoController> controller);
    
protected:

    void On_buttonLight_clicked();
    void On_buttonMeasure_clicked();
    void On_scaleMotor_value_change_();

    Gtk::Box mainBox_;
    Gtk::Box boxButtonsH_;
    Glib::RefPtr<Gtk::Adjustment> adjustmentMotor_;
    Gtk::Scale scaleMotor_;

    Gtk::Label labelDistance_;

    Gtk::ToggleButton buttonLight_;
    Gtk::Button buttonMeasure_;

    boost::shared_ptr<ArduinoController> controller_;
private:
    void UpdateDistance();
    void SetLight();
};
#endif 
