#ifndef __PARSER__
#define __PARSER__

#include <String>
#include <vector>

class Parser
{
public:
    static bool ParseDouble(const std::string &str, double &r);

    static bool ParseDoubleList(const std::string &str, std::vector<double> &v); 
};

#endif
