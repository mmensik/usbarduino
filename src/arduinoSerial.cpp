#include "arduinoSerial.hpp"
#include "arduino-serial-lib.hpp"


using namespace std;

const int ArduinoSerial::MAX_BUFFER = 256;

ArduinoSerial::ArduinoSerial(string port, int brate)
{
    mtx_.lock();
    fd_ = serialport_init(port.c_str(), brate);
    serialport_flush(fd_);
    mtx_.unlock();
}

string ArduinoSerial::ReadLine()
{
    mtx_.lock();
    char buf[MAX_BUFFER];
    serialport_read_until(fd_, buf, '\n', MAX_BUFFER, 5000);
    mtx_.unlock();
    return string(buf);
}

void ArduinoSerial::WriteByte(unsigned char byte)
{
    mtx_.lock();
    serialport_writebyte(fd_, byte);
    mtx_.unlock();
}

void ArduinoSerial::Write(const char *bytes)
{
    mtx_.lock();
    serialport_write(fd_, bytes);
    mtx_.unlock();
}
