#ifndef __ARDUINO_SERIAL__
#define __ARDUINO_SERIAL__

#include <String>
#include <boost/thread/mutex.hpp>

class IArduinoSerial
{
public:
    virtual std::string ReadLine() = 0;

    virtual void WriteByte(unsigned char byte) = 0;

    virtual void Write(const char *bytes) = 0;
};

class ArduinoSerial : public IArduinoSerial
{
public:
    ArduinoSerial(std::string port, int brate = 9600);        

    virtual std::string ReadLine();

    virtual void WriteByte(unsigned char byte);

    virtual void Write(const char *bytes);
private:
    static const int MAX_BUFFER;

    int fd_;

    boost::mutex mtx_;
};

class MockedArduinoSerial : public IArduinoSerial
{
public:
    virtual std::string ReadLine() { return "1.34"; }

    virtual void WriteByte(unsigned char byte) { std::cout << "Byte: " << (int)byte << std::endl; }

    virtual void Write(const char *bytes) { std::cout << bytes << std::endl; }
};

#endif

