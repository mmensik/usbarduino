#include <String>
#include <iostream>
#include <unistd.h>   // UNIX standard function definitions 
#include <gtkmm.h>

#include <boost/shared_ptr.hpp>

#include "arduinoSerial.hpp"
#include "arduinoController.hpp"

#include "testWindow.hpp"

using namespace std;

int main(int argc, char *argv[])
{

    boost::shared_ptr<ArduinoSerial> serial(new ArduinoSerial("/dev/tty.usbmodem1421"));
    boost::shared_ptr<ArduinoController> controller(new ArduinoController(serial));
    
    cout << controller->ReadDistance() << endl;
    Glib::RefPtr<Gtk::Application> app =
        Gtk::Application::create(argc, argv,
           "org.gtkmm.examples.base");
    TestWindow window(controller);

    return app->run(window);

}

